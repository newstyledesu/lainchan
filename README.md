# Lainchan Comfy Compact

## About
A compact theme for lainchan.org with a mascot. Includes a convenient fixed post form. You can replace the mascot with your own by replacing the base64 encoded image at the end of the css file.

## Links
Userstyles page: https://userstyles.org/styles/148556/lainchan-comfy-compact

## Screenshot
![Theme Screenshot](lainchan.png)

## Installation

### Step 1:
Install the Stylish or Stylus extension for Firefox or Chrome. Then, either:

### Step 2:
Install from userstyles (https://userstyles.org/styles/148556/lainchan-comfy-compact)  
OR  
Import the *.css file into Stylish or Stylus manually